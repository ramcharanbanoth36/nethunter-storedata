* Add USBArmory fragment for HID support of previouslu unsupported devices, e.g. OP7- @yesimxev
* Add "COMBO" command to duckyconverter - @yesimxev
* Add background location support for Android 10
* Considerable re-write by @simonpunk
* KeX manager by @yesimxev
* usbarmory script by @simonpunk
* Adds multi-user functionality to KeX manager - @yesimxev
! Multi-user KeX requires rootfs 2020.1 or newer
