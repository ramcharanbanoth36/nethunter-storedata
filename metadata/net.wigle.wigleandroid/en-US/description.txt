Open source network observation, positioning, and display client from the world's largest queryable database of wireless networks. Can be used for site-survey, security analysis, and competition with your friends. Collect networks for personal research or upload to https://wigle.net. WiGLE has been collecting and mapping network data since 2001, and currently has over 350m networks. WiGLE is *not* a list of networks you can use.

* Uses GPS to estimate locations of observed networks
* Observations logged to local database to track your networks found
* Upload and compete on the global WiGLE.net leaderboard
* Real-time map of networks found, with overlays from entire WiGLE dataset
* Free, open source, no ads (pull requestes welcome at https://github.com/wiglenet/wigle-wifi-wardriving )
* Export to CSV files on SD card (comma separated values)
* Export to KML files on SD card (to import into Google Maps/Earth)
* Bluetooth GPS support through mock locations
* Audio and Text-to-Speech alerting and "Mute" option to shut off all sound/speech

Current release notes notes: https://github.com/wiglenet/wigle-wifi-wardriving/blob/master/TODO

Feedback and support requests are welcome via email wigle-admin@wigle.net, github, or https://wigle.net/phpbb/viewforum.php?f=13 (registration required).

You can help to improve our language files and translations via pull requests at:
https://github.com/wiglenet/wigle-wifi-wardriving or sending email to wigle-admin@wigle.net

Keep on stumbling!
 
