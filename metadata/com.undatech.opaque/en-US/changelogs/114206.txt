v1.5.2
- Security updates
- Stability fixes for Android 10
v1.5.1
- 64-bit support
- Bugfixes
v1.5.0
- Ability to specify custom port for PVE
- Support for SPICE server mouse mode
v1.4.9
- Bugfixes
v1.4.8
- Ability to copy logcat
v1.4.7
- Fix for scrolling in Single Handed mode
- Bugfixes for Proxmox
v1.4.6
- Fix for Menu + immersive mode live-lock on some devices
- Updated spice library
v1.4.5
- Input mode bugfixes
- Security and protocol library updates
v1.4.4
- New, relocatable toolbar
- Im